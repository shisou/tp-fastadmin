define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree'], function ($, undefined, Backend, Table, Form, undefined) {
    //读取选中的条目
    $.jstree.core.prototype.get_all_checked = function (full) {
        var obj = this.get_selected(), i, j;
        for (i = 0, j = obj.length; i < j; i++) {
            obj = obj.concat(this.get_node(obj[i]).parents);
        }
        obj = $.grep(obj, function (v, i, a) {
            return v != '#';
        });
        obj = obj.filter(function (itm, i, a) {
            return i == a.indexOf(itm);
        });
        
        return full ? $.map(obj, $.proxy(function (i) {
            return this.get_node(i);
        }, this)) : obj;
    };
    var Controller = {
        list: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: '/adm/auth/role/list',
                    add_url: '/adm/auth/role/edit',
                    edit_url: '/adm/auth/role/edit',
                    del_url: '/adm/auth/role/del',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                escape: false,
                pagination: false,
                search: false,
                commonSearch: false,
                showExport:false,
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID'},
                        // {field: 'pid', title: __('Parent')},
                        {field: 'name', title: __('Name'), align: 'left'},
                        {
                            field: 'status',
                            title: __("Status"),
                            // searchList:{'0':'禁用','1':'正常'},
                            formatter: function (value, row, index) {
                                if(row.status == 1){
                                    return '正常';
                                }else{
                                    return '禁用';
                                }
                            }
                        },
                        // {field: 'operate', title: __('Operate'), events: Table.api.events.operate, formatter: function (value, row, index) {
                        //         if(row.id == 1){
                        //             return '';
                        //         }
                        //         return Table.api.formatter.operate.call(this, value, row, index, table);
                        //     }}
                         {field: 'operate', title: __('Operate'), events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ],
                
            });

            // 为表格绑定事件
            Table.api.bindevent(table);//当内容渲染完成后

        },
        add: function () {
            Form.api.bindevent($("form[role=form]"));
            Controller.api.bindevent();
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
            Controller.api.bindevent();
        },
        api: {
            refreshrules: function () {
                if ($("#treeview").size() > 0) {
                    var r = $("#treeview").jstree("get_all_checked");
                    $("input[name='row[menu_ids]']").val(r.join(','));
                }
                if ($("#treeview1").size() > 0) {
                    var j = $("#treeview1").jstree("get_all_checked");
                    $("input[name='row[cate_ids]']").val(j.join(','));
                }
                return true;
            },
            bindevent: function () {
                Form.api.custom.refreshrules = Controller.api.refreshrules;
                //渲染权限节点树
                //变更级别后需要重建节点树
                $.ajax({
                    url: "/adm/auth/role/menu",
                    type: 'post',
                    dataType: 'json',
                    data:{id:$("#id").val()},
                    success: function (ret) {
                        if (ret.hasOwnProperty("code")) {
                            var data = ret.hasOwnProperty("data") && ret.data != "" ? ret.data : "";
                            if (ret.code === 1) {
                                //销毁已有的节点树
                                $("#treeview").jstree("destroy");
                                Controller.api.rendertree(data);
                            } else {
                                Backend.api.toastr.error(ret.data);
                            }
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });

                $.ajax({
                    url: "/adm/auth/role/cate",
                    type: 'post',
                    dataType: 'json',
                    data:{id:$("#id").val()},
                    success: function (ret) {
                        if (ret.hasOwnProperty("code")) {
                            var data1 = ret.hasOwnProperty("data") && ret.data != "" ? ret.data : "";
                            if (ret.code === 1) {
                                //销毁已有的节点树
                                $("#treeview1").jstree("destroy");
                                Controller.api.rendertree1(data1);
                            } else {
                                Backend.api.toastr.error(ret.data);
                            }
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });

                //全选和展开
                $(document).on("click", "#checkall", function () {
                    $("#treeview").jstree($(this).prop("checked") ? "check_all" : "uncheck_all");
                });
                $(document).on("click", "#expandall", function () {
                    $("#treeview").jstree($(this).prop("checked") ? "open_all" : "close_all");
                });
                //全选和展开
                $(document).on("click", "#checkall1", function () {
                    $("#treeview1").jstree($(this).prop("checked") ? "check_all" : "uncheck_all");
                });
                $(document).on("click", "#expandall1", function () {
                    $("#treeview1").jstree($(this).prop("checked") ? "open_all" : "close_all");
                });
            },
            rendertree: function (content) {
                $("#treeview").on('redraw.jstree', function (e) {
                    $(".layer-footer").attr("domrefresh", Math.random());
                }).jstree({
                    "themes": {"stripes": true},
                    "checkbox": {
                        "keep_selected_style": false,
                    },
                    "types": {
                        "root": {
                            "icon": "fa fa-folder-open",
                        },
                        "menu": {
                            "icon": "fa fa-folder-open",
                        },
                        "file": {
                            "icon": "fa fa-file-o",
                        }
                    },
                    "plugins": ["checkbox", "types"],
                    "core": {
                        'check_callback': true,
                        "data": content
                    }
                });
            },
            rendertree1: function (content) {
                $("#treeview1").on('redraw.jstree', function (e) {
                    $(".layer-footer").attr("domrefresh", Math.random());
                }).jstree({
                    "themes": {"stripes": true},
                    "checkbox": {
                        "keep_selected_style": false,
                    },
                    "types": {
                        "root": {
                            "icon": "fa fa-folder-open",
                        },
                        "menu": {
                            "icon": "fa fa-folder-open",
                        },
                        "file": {
                            "icon": "fa fa-file-o",
                        }
                    },
                    "plugins": ["checkbox", "types"],
                    "core": {
                        'check_callback': true,
                        "data": content
                    }
                });
            }
        }
    };
    return Controller;
});