define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'template'], function ($, undefined, Backend, Table, Form, Template) {

    var Controller = {
        list: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: '/adm/auth/menu/list',
                    add_url: '/adm/auth/menu/edit',
                    edit_url: '/adm/auth/menu/edit',
                    del_url: '/adm/auth/menu/del',
                    // multi_url: 'data/multi.json',
                    // "table": "auth_rule"
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                sortName: 'sort',
                escape:false, 
                pagination: false,
                search: false,
                commonSearch: false,
                clickToSelect: false, //是否启用点击选中
                dblClickToEdit: false, //是否启用双击编辑，改为 false 即可。
                showExport:false,
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID'},
                        {field: 'title', title: __('Title'), align: 'left', align: 'left', formatter: Controller.api.formatter.title},
                        {field: 'icon', title: __('Icon'), formatter: Controller.api.formatter.icon},
                        {field: 'url', title: 'URL', align: 'left', formatter: Controller.api.formatter.name},
                        // {field: 'sort', title: __('Weigh')},
                        {
                            field: 'status',
                            title: __("Status"),
                            // searchList:{'0':'禁用','1':'正常'},
                            formatter: function (value, row, index) {
                                if(row.status == 1){
                                    return '正常';
                                }else{
                                    return '禁用';
                                }
                            }
                        },
                        // {field: 'is_menu', title: __('Ismenu'), align: 'center', formatter: Controller.api.formatter.menu},
                        {field: 'id', title: '<a href="javascript:;" class="btn btn-success btn-xs btn-toggle"><i class="fa fa-chevron-up"></i></a>', operate: false, formatter: Controller.api.formatter.subnode},
                        // {field: 'id', title: '点击展开', operate: false, formatter: Controller.api.formatter.subnode},
                        {field: 'operate', title: __('Operate'), events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ],
                
            });

            // 为表格绑定事件
            Table.api.bindevent(table);//当内容渲染完成后

            //默认隐藏所有子节点
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                //$("a.btn[data-id][data-pid][data-pid!=0]").closest("tr").hide();
                $(".btn-node-sub.disabled").closest("tr").hide();

                //显示隐藏子节点
                $(".btn-node-sub").off("click").on("click", function (e) {
                    var status = $(this).data("shown") ? true : false;
                    $("a.btn[data-pid='" + $(this).data("id") + "']").each(function () {
                        $(this).closest("tr").toggle(!status);
                    });
                    $(this).data("shown", !status);
                    return false;
                });

            });
            //展开隐藏一级
            $(document.body).on("click", ".btn-toggle", function (e) {
                $("a.btn[data-id][data-pid][data-pid!=0].disabled").closest("tr").hide();
                var that = this;
                var show = $("i", that).hasClass("fa-chevron-down");
                $("i", that).toggleClass("fa-chevron-down", !show);
                $("i", that).toggleClass("fa-chevron-up", show);
                $("a.btn[data-id][data-pid][data-pid!=0]").not('.disabled').closest("tr").toggle(show);
                $(".btn-node-sub[data-pid=0]").data("shown", show);
            });
            //展开隐藏全部
            $(document.body).on("click", ".btn-toggle-all", function (e) {
                var that = this;
                var show = $("i", that).hasClass("fa-plus");
                $("i", that).toggleClass("fa-plus", !show);
                $("i", that).toggleClass("fa-minus", show);
                $(".btn-node-sub.disabled").closest("tr").toggle(show);
                $(".btn-node-sub").data("shown", show);
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            formatter: {
                title: function (value, row, index) {
                    let str = '';
                    if(row.type == 2){
                        str = '&nbsp;&nbsp;&nbsp;&nbsp;|--&nbsp;&nbsp;'
                    }else if(row.type == 3){
                        str = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|---&nbsp;&nbsp;'
                    }else if(row.type == 4){
                        str = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|------&nbsp;&nbsp;'
                    }
                    var span = "<span class='text-muted'>" + str + value + "</span>";

                    if(row.children != ''){
                        span = str + value
                    }

                    return span;
                },
                name: function (value, row, index) {
                    var span = "<span class='text-muted'>" + value + "</span>";
                    if(row.children == ''){
                        span = value
                    }
                    return span;
                },
                menu: function (value, row, index) {
                    return "<a href='javascript:;' class='btn btn-" + (value ? "info" : "default") + " btn-xs btn-change' data-id='"
                            + row.id + "' data-params='ismenu=" + (value ? 0 : 1) + "'>" + (value ? __('Yes') : __('No')) + "</a>";
                },
                icon: function (value, row, index) {
                    return '<i class="' + value + '"></i>';
                },
                subnode: function (value, row, index) {
                    var class1 = 'btn-success';

                    if(row.children == ''){
                        class1 = 'btn-default disabled'
                    }

                    return '<a href="javascript:;" data-id="' + row['id'] + '" data-pid="' + row['parent_id'] + '" class="btn btn-xs ' + class1 + ' btn-node-sub"><i class="fa fa-sitemap"></i></a>';
                }
            },
            bindevent: function () {
                var iconlist = [];
                Form.api.bindevent($("form[role=form]"));
                $(document).on('click', ".btn-search-icon", function () {
                    if (iconlist.length == 0) {
                        $.get("/adm/public/ico", function (ret) {
                            var exp = /fa-var-(.*):/ig;
                            var result;
                            while ((result = exp.exec(ret)) != null) {
                                iconlist.push(result[1]);
                            }
                            Layer.open({
                                type: 1,
                                area: ['460px', '300px'], //宽高
                                content: Template('chooseicontpl', {iconlist: iconlist})
                            });
                        });
                    } else {
                        Layer.open({
                            type: 1,
                            area: ['460px', '300px'], //宽高
                            content: Template('chooseicontpl', {iconlist: iconlist})
                        });
                    }
                });
                $(document).on('click', '#chooseicon ul li', function () {
                    $("input[name='row[icon]']").val('fa fa-' + $(this).data("font"));
                    $("#icon-style").attr('class', 'fa fa-' + $(this).data("font"));
                   
                    Layer.closeAll();
                });
                $(document).on('keyup', 'input.js-icon-search', function () {
                    $("#chooseicon ul li").show();
                    if ($(this).val() != '') {
                        $("#chooseicon ul li:not([data-font*='" + $(this).val() + "'])").hide();
                    }
                });
            }
        }
    };
    return Controller;
});