define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'bootstrap-datetimepicker'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        list: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: '/adm/message/list',
                    del_url: '/adm/message/del',
                    table: 'message',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                // dataField: 'data',
                // sortName: 'sort',
                // pagination: false, 
                // commonSearch: false,
                showExport: false,
                striped: true, //是否显示行间隔色
                search: false,
                columns: [
                    [{
                        checkbox: true
                    },
                    {
                        field: 'id',
                        title: __('Id'),
                        sortable: true,
                        operate: false
                    },
                    {
                        field: 'name',
                        title: '姓名',
                        operate: 'LIKE',
                        placeholder: '模糊搜索'
                    },
                    {
                        field: 'mobile',
                        title: '手机',
                        operate: 'LIKE',
                        placeholder: '模糊搜索'
                    },
                    { field: 'content', title: '留言', operate: false },
                    {
                        field: 'created_at',
                        sortable: true,
                        title: '提交时间',
                        operate: false,
                        formatter: function (value, row, index) {
                            if (row.created_at) {
                                return row.created_at;
                            }
                        }
                    },
                    {
                        field: 'operate',
                        title: __('Operate'),
                        events: Table.api.events.operate,
                        formatter: Table.api.formatter.operate
                    }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                $(".datetimeday").datetimepicker({
                    format: 'YYYY-MM-DD HH:mm',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-history',
                        clear: 'fa fa-trash',
                        close: 'fa fa-remove'
                    },
                    showTodayButton: true,
                    showClose: true
                });
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});