var u = document.querySelector(".my-content");
if(u){
    u.addEventListener("paste", function(e) {
        e.stopPropagation();
        e.preventDefault();
        var text = '',
            event = (e.originalEvent || e);
        
        if (event.clipboardData && event.clipboardData.getData) {
            text = event.clipboardData.getData('text/plain');
        } else if (window.clipboardData && window.clipboardData.getData) {
            text = window.clipboardData.getData('Text');
        }

        if (document.queryCommandSupported('insertText')) {
            document.execCommand('insertText', false, text);
        } else {
            document.execCommand('paste', false, newContent);
        }
    });
}
document.execCommand('defaultParagraphSeparator', false, 'p');

function  contenteditable(obj) {
    obj.append("<p>&#8203;</p>");
    obj.on('focus', function() {
        var selection = document.getSelection();
        var range = document.createRange();
        obj.append("<p>&#8203;</p>");
        var insertedElement = obj.find('p');
        range.setStart(insertedElement[0].firstChild, 1);
        selection.removeAllRanges();
        selection.addRange(range);
    });
}
            
