define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'bootstrap-datetimepicker'], function($, undefined, Backend, Table, Form) {

    var Controller = {
        list: function() {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: '/adm/tags/list',
                    add_url: '/adm/tags/edit',
                    edit_url: '/adm/tags/edit',
                    del_url: '/adm/tags/del',
                    table: 'tags',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                // dataField: 'data',
                sortName: 'sort',
                // pagination: false, 
                // commonSearch: false,
                showExport: false,
                striped: true, //是否显示行间隔色
                search: false,
                columns: [
                    [{
                            checkbox: true
                        },
                        {
                            field: 'id',
                            title: __('Id'),
                            sortable: true,
                            operate: false
                        },
                        {
                            field: 'cate_id',
                            title: '栏目',
                            searchList: $.getJSON('/adm/public/cate'),
                            formatter: function(value, row, index) {
                                if(row.cate){
                                    return row.cate.title;
                                }
                                return '';
                            }
                        },
                        {
                            field: 'sort',
                            title: __('Sort'),
                            operate: false,
                            width: 10,
                            sortable: true
                        },
                        
                        {
                            field: 'name',
                            title: '标签',
                            operate: 'LIKE',
                            placeholder: '模糊搜索'
                        },
                        
                        {
                            field: 'operate',
                            title: __('Operate'),
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function() {
            Controller.api.bindevent();
        },
        edit: function() {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function() {
                $(".datetimeday").datetimepicker({
                    format: 'YYYY-MM-DD HH:mm',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-history',
                        clear: 'fa fa-trash',
                        close: 'fa fa-remove'
                    },
                    showTodayButton: true,
                    showClose: true
                });
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});