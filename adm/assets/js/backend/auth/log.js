define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function($, undefined, Backend, Table, Form) {

    var Controller = {
        list: function() {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: '/adm/auth/log/list',
                    // add_url: '',
                    // edit_url: '',
                    del_url: '/adm/auth/log/del',
                    // multi_url: 'data/multi.json',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                 escape:false, 
                pagination: false,
                search: false,
                // commonSearch: false,
                clickToSelect: false, //是否启用点击选中
                dblClickToEdit: false, //是否启用双击编辑，改为 false 即可。
                showExport:false,
                columns: [
                    [{
                            field: 'state',
                            checkbox: true,
                        },
                        {
                            field: 'id',
                            title: 'ID',
                            operate: false
                        },
                        {
                            field: 'username',
                            title: __('Username'),
                            formatter: function (value, row, index) {
                                return row.admin.username;
                            }
                        },
                        {
                            field: 'title',
                            title: __('Title'),
                            operate: 'LIKE %...%',
                            placeholder: '模糊搜索，*表示任意字符',
                            style: 'width:200px'
                        },
                        {
                            field: 'url',
                            title: __('Url'),
                            align: 'left',
                            formatter: Controller.api.formatter.url
                        },
                        {
                            field: 'ip',
                            title: __('IP'),
                            events: Controller.api.events.ip,
                            formatter: Controller.api.formatter.ip
                        },
                        {
                            field: 'browser',
                            title: __('Browser'),
                            operate: false,
                            events: Controller.api.events.browser,
                            formatter: Controller.api.formatter.browser
                        },
                        {
                            field: 'created_at',
                            title: __('Create time'),
                            operate: 'BETWEEN',
                            type: 'datetime',
                            addclass: 'datetimepicker',
                            data: 'data-date-format="YYYY-MM-DD HH:mm:ss"'
                        },
                        {
                            field: 'operate',
                            title: __('Operate'),
                            events: Controller.api.events.operate,
                            formatter: Controller.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        api: {
            bindevent: function() {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                url: function(value, row, index) {
                    return '<div class="input-group input-group-sm" style="width:250px;"><input type="text" class="form-control input-sm" value="' + value + '"><span class="input-group-btn input-group-sm"><a href="' + value + '" target="_blank" class="btn btn-default btn-sm"><i class="fa fa-link"></i></a></span></div>';
                },
                ip: function(value, row, index) {
                    return '<a class="btn btn-xs btn-ip bg-success"><i class="fa fa-map-marker"></i> ' + value + '</a>';
                },
                browser: function(value, row, index) {
                    return '<a class="btn btn-xs btn-browser">' + row.useragent.split(" ")[0] + '</a>';
                },
                operate: function(value, row, index) {
                    return '<a class="btn btn-info btn-xs btn-detail">' + __('Detail') + '</a> ' +
                        Table.api.formatter.operate(value, row, index, $("#table"));
                },
            },
            events: {
                ip: {
                    'click .btn-ip': function(e, value, row, index) {
                        var options = $("#table").bootstrapTable('getOptions');
                        //这里我们手动将数据填充到表单然后提交
                        $("#commonSearchContent_" + options.idTable + " form [name='ip']").val(value);
                        $("#commonSearchContent_" + options.idTable + " form").trigger('submit');
                    }
                },
                operate: $.extend({
                    'click .btn-detail': function(e, value, row, index) {
                        Backend.api.open('/adm/auth/log/detail?ids=' + row['id'], __('Detail'),600,400);
                    }
                }, Table.api.events.operate)
            }
        }
    };
    return Controller;
});