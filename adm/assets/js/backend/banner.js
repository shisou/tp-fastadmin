define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function($, undefined, Backend, Table, Form) {

    var Controller = {
        list: function() {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: '/adm/banner/list',
                    add_url: '/adm/banner/edit',
                    edit_url: '/adm/banner/edit',
                    del_url: '/adm/banner/del',
                    multi_url: '',
                    disabled_url: '',
                    table: 'banner',
                }
            });

            var table = $("#table");
            var tableOptions = {
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'sort',
                // pagination: false,
                commonSearch: false,
                search: false,
                showExport:false,
                columns: [
                    [{
                            checkbox: true
                        },
                        {
                            field: 'id',
                            title: __('Id'),
                            operate: false
                        },
                        {
                            field:  'position', 
                            title: 'web/手机端',
                           searchList:{'0':'web端','1':'手机端'},
                           operate: false,
                            formatter: function(value, row, index) {
                                if (row.position == 1) {
                                    return '手机端';
                                } else {
                                    return 'web端';
                                }
                            }
                        },
                        {
                            field: 'type',
                            title: __('Type'),
                            // searchList:{'1':'启动页','2':'首页背景'},
                            operate: false,
                            formatter: function(value, row, index) {
                                if (row.type == 1) {
                                    return '封面图';
                                } else {
                                    return '视频';
                                }
                            }
                        },
                        {
                            field: 'img_cover',
                            title: '封面图',
                            operate: false,
                            events: Table.api.events.image,
                            formatter: Table.api.formatter.image
                        },
                        {
                            field: 'url', 
                            title: __('Url'),
                            operate: false, 
                            formatter: Table.api.formatter.url
                        },
                        {
                            field: 'sort',
                            title: __('Sort'),
                            operate: false,
                            width: 10,
                            sortable: true
                        },
                        {
                            field: 'status',
                            title: __('Status'),
                            operate: false,
                            formatter: function(value, row, index) {
                                if (row.status == 1) {
                                    return '<span class="btn btn-success btn-editone btn-xs">正常</span>';
                                } else {
                                    return '<span class="btn btn-danger btn-delone btn-xs">禁用</span>';
                                }
                            }
                        },
                        {
                            field: 'operate',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            formatter: function(params) {
                                // return '<a href="javascript:;" class="btn btn-primary btn-dragsort btn-xs"><i class="fa fa-arrows"></i></a>'
                                return '<a href="javascript:;" class="btn btn-success btn-editone btn-xs"><i class="fa fa-pencil"></i>&nbsp;修改</a>&nbsp;&nbsp;' +
                                    '<a href="javascript:;" class="btn btn-danger btn-delone btn-xs"><i class="fa fa-trash"></i>&nbsp;删除</a>'
                            }
                        }
                    ]
                ]
            };
            // 初始化表格
            table.bootstrapTable(tableOptions);

            // 为表格绑定事件
            Table.api.bindevent(table);

            //绑定TAB事件
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                // var options = table.bootstrapTable(tableOptions);
                var typeStr = $(this).attr("href").replace('#', '');
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function(params) {
                    // params.filter = JSON.stringify({type: typeStr});
                    params.type = typeStr;

                    return params;
                };
                table.bootstrapTable('refresh', {});
                return false;

            });

            //必须默认触发shown.bs.tab事件
            // $('ul.nav-tabs li.active a[data-toggle="tab"]').trigger("shown.bs.tab");
        },
        add: function() {
            Form.api.bindevent($("form[role=form]"));

        },
        edit: function() {
            Form.api.bindevent($("form[role=form]"));
        }
    };
    return Controller;
});