define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'bootstrap-datetimepicker'], function($, undefined, Backend, Table, Form) {

    var Controller = {
        list: function() {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: '/adm/news/list',
                    add_url: '/adm/news/edit',
                    edit_url: '/adm/news/edit',
                    del_url: '/adm/news/del',
                    table: 'news',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                // dataField: 'data',
                sortName: 'sort',
                // pagination: false, 
                // commonSearch: false,
                showExport: false,
                striped: true, //是否显示行间隔色
                search: false,
                columns: [
                    [{
                            checkbox: true
                        },
                        {
                            field: 'id',
                            title: __('Id'),
                            sortable: true,
                            operate: false
                        },
                        {
                            field: 'sort',
                            title: __('Sort'),
                            operate: false,
                            width: 10,
                            sortable: true
                        },
                        {
                            field: 'cate_id',
                            title: '栏目',
                            searchList: $.getJSON('/adm/public/cate'),
                            formatter: function(value, row, index) {
                                if(row.cate){
                                    return row.cate.title;
                                }
                                return '';
                            }
                        },
                        {
                            field: 'title',
                            title: '标题',
                            operate: 'LIKE',
                            placeholder: '模糊搜索'
                        },
                        {
                            field: 'from',
                            title: '来源',
                            operate: 'LIKE',
                            placeholder: '模糊搜索'
                        },
                        {
                            field: 'img_cover',
                            title: __('Image'),
                            operate: false,
                            events: Table.api.events.image,
                            formatter: Table.api.formatter.image
                        },
                        // {field: 'description', title:'简介',operate: false},
                        {
                            field: 'add_time',
                            sortable: true,
                            title: '发布时间',
                            operate: false,
                            formatter: function(value, row, index) {
                                if (row.add_time) {
                                    return row.add_time;
                                }
                            }
                        },
                        {
                            field: 'is_rec',
                            title: '推荐',
                            operate: false,
                            formatter: function(value, row, index) {
                                if (row.is_rec == 3) {
                                    return '<span class="btn btn-danger btn-delone btn-xs">置顶</span>';
                                   
                                } else if(row.is_rec == 2){
                                    return '<span class="btn btn-warning btn-editone btn-xs">头条</span>';
                                }else if(row.is_rec == 1){
                                    return '<span class="btn btn-primary btn-editone btn-xs">推荐</span>';
                                }else {
                                    return '<span class="btn btn-success btn-editone btn-xs"></span>';
                                }
                                
                            }
                        },
                        {
                            field: 'status',
                            title: __('Status'),
                            searchList: { '1': '正常', '2': '被驳回', '9': '待审核', '0': '隐藏'},
                            formatter: function(value, row, index) {
                                if (row.status == 1) {
                                    return '<span class="btn btn-success btn-editone btn-xs">正常</span>';
                                } else if(row.status == 2){
                                    return '<span class="btn btn-warning btn-editone btn-xs">被驳回</span>';
                                }else if(row.status == 9){
                                    return '<span class="btn btn-primary btn-editone btn-xs">待审核</span>';
                                }else {
                                    return '<span class="btn btn-danger btn-delone btn-xs">隐藏</span>';
                                }
                            }
                        },
                        {
                            field: 'operate',
                            title: __('Operate'),
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function() {
            Controller.api.bindevent();
        },
        edit: function() {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function() {
                $(".datetimeday").datetimepicker({
                    format: 'YYYY-MM-DD HH:mm',
                    icons: {
                        time: 'fa fa-clock-o',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-history',
                        clear: 'fa fa-trash',
                        close: 'fa fa-remove'
                    },
                    showTodayButton: true,
                    showClose: true
                });
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});