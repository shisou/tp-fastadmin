define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        list: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: '/adm/auth/admin/list',
                    add_url: '/adm/auth/admin/edit',
                    edit_url: '/adm/auth/admin/edit',
                    del_url: '/adm/auth/admin/del',
                    // multi_url: '/adm/auth/admin/del',
                    // dragsort_url:'', //拖拽
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                method: 'post',
                pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
                pk: 'id',
                responseHandler: function (res) {
                    console.log(res)
                    return res;
                },
                search:false,
                showExport:false,
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID',operate: false},
                        {
                            field: 'username',
                            title: __('Username'),
                            operate: 'LIKE',
                            placeholder: '模糊搜索',
                            style: 'width:200px'
                        },

                        {
                            field: 'mobile',
                            title: __('Mobile')
                        },

                        {
                            field: 'status',
                            title: __("Status"),
                            searchList:{'0':'禁用','1':'正常'},
                            formatter: function (value, row, index) {
                                if(row.status == 1){
                                    return '正常';
                                }else{
                                    return '禁用';
                                }
                            }
                        },
                        {
                            field: 'created_at',
                            title: __("Created_at"),
                            operate: 'BETWEEN',
                            type: 'datetime',
                            addclass: 'datetimepicker',
                            data: 'data-date-format="YYYY-MM-DD HH:mm:ss"'
                        },
                        {field: 'operate', title: __('Operate'), events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Form.api.bindevent($("form[role=form]"));
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
        }
    };
    return Controller;
});