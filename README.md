#### 如需使用，请在您的composer.json 添加以下配置项
- installer-paths：安装路径/安装包名字
- 执行 composer update

```
{
    "require": {
        "shisou/tp-fastadmin": "^1.0",
        "oomphinc/composer-installers-extender": "2.0.*"
    },
    "extra": {
        "installer-types": [
            "project"
        ],
        "installer-paths": {
            "public/static": [
                "shisou/tp-fastadmin"
            ]
        }
    }
}
```


FastAdmin<sup>HTML</sup>是一款基于Bootstrap的极速后台开发模板。
===============


## **主要特性**

* 基于`AdminLTE`二次开发
* 基于`Bootstrap`开发，自适应手机、平板、PC
* 基于`RequireJS`进行JS模块管理，按需加载
* 基于`Less`进行样式开发
* 基于`Bower`进行前端组件包管理
* 基于第三方插件的增删改查一站式整合方案
* 一键压缩打包JS和CSS文件
* 多语言客户端支持
* 无缝整合又拍云上传功能


## 使用说明

- adm文件内不应存在html，初始 html 可以删除。如使用请将 html 放置指定框架文件夹内
- 请严格按照以下路径说明进行使用


#### js

- adm/assets/js/backend
- 请将 ****.js 放置此目录下


#### css

- adm/assets/css
- 请将 ****.css 放置此目录下

#### fonts

- adm/assets/fonts
- 请将 ****.css 放置此目录下


#### img

- adm/assets/img
- 请将 ****.img 放置此目录下

